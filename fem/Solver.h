#pragma once

#include "Mesh.h"

std::vector<float> solveStressStrainProblem(const Mesh& mesh, const std::vector<Point>& forces);
