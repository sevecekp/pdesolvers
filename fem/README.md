# Solution of a strain-stress problem using finite element method

Example code written for lecture 'Selected chapters on astrophysics' (fall/winter 2019).

## Requirements

To compile the code, you need:

 - Compiler `gcc >= 6.0`
 - `wxWidgets` library (on Debian/Ubuntu systems, install package `libwxgtk3.0-dev`)
 - `OpenMP` (package `libomp-dev`)
 - `Eigen` (package `libeigen3-dev`)
 - `GNU MP` (package `libgmp-dev`)
 - `CGAL` (package `libcgal-dev`)

## Compilation

Simply do:
``` make ```

