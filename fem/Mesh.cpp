#include "Mesh.h"
#include <CGAL/Constrained_Delaunay_triangulation_2.h>
#include <CGAL/Delaunay_mesh_face_base_2.h>
#include <CGAL/Delaunay_mesh_size_criteria_2.h>
#include <CGAL/Delaunay_mesher_2.h>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Polygon_2.h>

typedef CGAL::Exact_predicates_inexact_constructions_kernel Kernel;
typedef CGAL::Triangulation_vertex_base_2<Kernel> VertexBase;
typedef CGAL::Delaunay_mesh_face_base_2<Kernel> FaceBase;
typedef CGAL::Triangulation_data_structure_2<VertexBase, FaceBase> Triangulation;
typedef CGAL::Constrained_Delaunay_triangulation_2<Kernel, Triangulation> Delaunay;
typedef CGAL::Delaunay_mesh_size_criteria_2<Delaunay> Criteria;

static Point toPoint(const Delaunay::Point_2 p) {
    return Point(p.x(), p.y());
}

Mesh constructMesh() {
    const float a = 2.f;
    const float b = 1.f;
    const float dx = 0.4f;
    const float r = 0.4f;

    auto inCircle = [r, dx](const Point& p) { return length(p - Point(dx, 0)) < r - 1.e-3; };

    Delaunay delaunay;
    std::vector<Delaunay::Vertex_handle> handles;
    for (float phi = 0.f; phi < 2.f * M_PI; phi += 0.01f) {
        handles.push_back(delaunay.insert(Delaunay::Point(a * cos(phi), b * sin(phi))));
    }
    for (uint32_t i = 1; i < handles.size(); ++i) {
        delaunay.insert_constraint(handles[i - 1], handles[i]);
    }
    delaunay.insert_constraint(handles.back(), handles.front());
    const int offset = handles.size();

    for (float phi = 0.f; phi < 2.f * M_PI; phi += 0.01f) {
        handles.push_back(delaunay.insert(Delaunay::Point(r * cos(phi) + dx, r * sin(phi))));
    }
    for (uint32_t i = offset + 1; i < handles.size(); ++i) {
        delaunay.insert_constraint(handles[i - 1], handles[i]);
    }
    delaunay.insert_constraint(handles.back(), handles[offset]);

    CGAL::refine_Delaunay_mesh_2(delaunay, Criteria(0.125, 0.1));

    Mesh mesh;
    std::map<Point, int> pointIndexMap;
    std::map<Point, int>::iterator pointIndexIter;

    for (Delaunay::Finite_faces_iterator iter = delaunay.finite_faces_begin();
         iter != delaunay.finite_faces_end();
         ++iter) {
        Delaunay::Vertex_handle v1 = iter->vertex(0);
        Delaunay::Vertex_handle v2 = iter->vertex(1);
        Delaunay::Vertex_handle v3 = iter->vertex(2);

        std::array<Point, 3> ps;
        ps[0] = toPoint(v1->point());
        ps[1] = toPoint(v2->point());
        ps[2] = toPoint(v3->point());

        if (inCircle(ps[0]) || inCircle(ps[1]) || inCircle(ps[2])) {
            continue;
        }

        std::array<int, 3> is;

        for (int i = 0; i < 3; ++i) {
            bool inserted;
            std::tie(pointIndexIter, inserted) =
                pointIndexMap.insert(std::make_pair(ps[i], mesh.nodes.size()));
            if (inserted) {
                // new vertex
                is[i] = mesh.nodes.size();
                mesh.nodes.push_back(ps[i]);
            } else {
                // existing vertex
                assert(mesh.nodes[pointIndexIter->second] == pointIndexIter->first);
                is[i] = pointIndexIter->second;
            }
        }

        mesh.faces.push_back(Face{ is[0], is[1], is[2] });
    }

    // find boundary nodes
    for (uint32_t i = 0; i < mesh.nodes.size(); ++i) {
        const Point p = Point(mesh.nodes[i][0] / a, mesh.nodes[i][1] / b);
        if (length(p) > 0.98f) {
            mesh.boundary.push_back(i);
        }
    }

    return mesh;
}

std::vector<Point> computeForces(const Mesh& mesh) {
    std::vector<Point> forces(mesh.nodes.size());
    for (uint32_t i = 0; i < mesh.nodes.size(); ++i) {
        const float w1 = exp(-length(mesh.nodes[i] - Point(0.4f, 0.4f)) / 0.1f);
        const float w2 = exp(-length(mesh.nodes[i] - Point(0.4f, -0.4f)) / 0.1f);
        forces[i] = Point(0.f, w1 - w2);
    }
    return forces;
}
