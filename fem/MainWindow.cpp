#include "MainWindow.h"

MainWindow::MainWindow()
    : wxFrame(nullptr, wxID_ANY, "FEM", wxDefaultPosition, wxSize(1024, 768)) {
    palette_ = Palette({
        { -2.f, Color(0, 0, 0.2f) },
        { -1.f, Color(0, 0, 1.f) },
        { 0.f, Color(0.5f, 0.5f, 0.5f) },
        { 1.f, Color(1.f, 0, 0) },
        { 2.f, Color(1.f, 1.f, 0) },
    });
    Bind(wxEVT_PAINT, [this](wxPaintEvent&) {
        if (mesh_.nodes.empty()) {
            return;
        }

        paint();
    });
}

void MainWindow::paint() {
    wxPaintDC dc(this);
    const wxSize size = dc.GetSize();

    Point p_min, p_max;
    std::tie(p_min, p_max) = mesh_.getExtents();
    const float scaleX = size.x / (p_max[0] - p_min[0]);
    const float scaleY = size.y / (p_max[1] - p_min[1]);

    for (uint32_t i = 0; i < mesh_.faces.size(); ++i) {
        std::array<wxPoint, 3> ps;
        for (int j = 0; j < 3; ++j) {
            const Point point = mesh_.nodes[mesh_.faces[i][j]];
            ps[j] = wxPoint((point[0] - p_min[0]) * scaleX, (point[1] - p_min[1]) * scaleY);
        }

        wxBrush brush = dc.GetBrush();
        brush.SetColour(palette_(u_[i]));
        dc.SetBrush(brush);
        dc.SetPen(*wxBLACK_PEN);

        dc.DrawPolygon(3, ps.data());
    }
}

void MainWindow::update(const Mesh& mesh, const std::vector<float>& u) {
    mesh_ = mesh;
    u_ = u;
    this->Refresh();
    wxYield();
}
