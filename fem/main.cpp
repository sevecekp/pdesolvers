#include "MainWindow.h"
#include "Mesh.h"
#include "Solver.h"
#include <wx/app.h>
#include <wx/msgdlg.h>

class App : public wxApp {
public:
    App() = default;

private:
    virtual bool OnInit() override;

    virtual int OnExit() override;
};

IMPLEMENT_APP(App);

bool App::OnInit() {
    MainWindow* window = new MainWindow();
    window->Show();

    Mesh mesh = constructMesh();
    std::vector<Point> forces = computeForces(mesh);
    std::vector<float> u = solveStressStrainProblem(mesh, forces);

    window->update(mesh, u);

    return true;
}

int App::OnExit() {
    return 0;
}
