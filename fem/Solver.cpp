#include "Solver.h"
#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Sparse>

static Eigen::Matrix3f getElasticityMatrix() {
    const float poissonRatio = 0.3f;
    const float youngModulus = 2000.f;
    Eigen::Matrix3f D;
    D << 1.0f, poissonRatio, 0.0f, //
        poissonRatio, 1.0, 0.0f,   //
        0.0f, 0.0f, (1.0f - poissonRatio) / 2.0f;
    D *= youngModulus / (1.0f - poissonRatio * poissonRatio);
    return D;
}

void computeStiffnessMatrix(const Mesh& mesh,
    std::vector<Eigen::Matrix<float, 3, 6>>& gradients,
    std::vector<Eigen::Triplet<float>>& triplets) {
    Eigen::Matrix3f D = getElasticityMatrix();
    for (uint32_t i = 0; i < mesh.faces.size(); ++i) {
        const Face& face = mesh.faces[i];

        Eigen::Vector3f x(mesh.nodes[face[0]][0], mesh.nodes[face[1]][0], mesh.nodes[face[2]][0]);
        Eigen::Vector3f y(mesh.nodes[face[0]][1], mesh.nodes[face[1]][1], mesh.nodes[face[2]][1]);

        Eigen::Matrix3f C;
        C << Eigen::Vector3f(1.f, 1.f, 1.f), x, y;

        Eigen::Matrix3f C_inv = C.inverse();

        Eigen::Matrix<float, 3, 6> B;
        for (int i = 0; i < 3; i++) {
            B(0, 2 * i + 0) = C_inv(1, i);
            B(0, 2 * i + 1) = 0.0f;
            B(1, 2 * i + 0) = 0.0f;
            B(1, 2 * i + 1) = C_inv(2, i);
            B(2, 2 * i + 0) = C_inv(2, i);
            B(2, 2 * i + 1) = C_inv(1, i);
        }

        gradients.push_back(B);

        Eigen::Matrix<float, 6, 6> K = B.transpose() * D * B * std::abs(C.determinant()) / 2.0f;

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                Eigen::Triplet<float> tri11(2 * face[i] + 0, 2 * face[j] + 0, K(2 * i + 0, 2 * j + 0));
                Eigen::Triplet<float> tri12(2 * face[i] + 0, 2 * face[j] + 1, K(2 * i + 0, 2 * j + 1));
                Eigen::Triplet<float> tri21(2 * face[i] + 1, 2 * face[j] + 0, K(2 * i + 1, 2 * j + 0));
                Eigen::Triplet<float> tri22(2 * face[i] + 1, 2 * face[j] + 1, K(2 * i + 1, 2 * j + 1));

                triplets.push_back(tri11);
                triplets.push_back(tri12);
                triplets.push_back(tri21);
                triplets.push_back(tri22);
            }
        }
    }
}

static void applyBoundaryConditions(Eigen::SparseMatrix<float>& K, const std::vector<int>& constraints) {
    std::vector<int> indicesToConstraint;

    for (int c : constraints) {
        indicesToConstraint.push_back(2 * c + 0);
        indicesToConstraint.push_back(2 * c + 1);
    }

    for (int k = 0; k < K.outerSize(); ++k) {
        for (Eigen::SparseMatrix<float>::InnerIterator it(K, k); it; ++it) {
            for (int index : indicesToConstraint) {
                if (it.row() == index || it.col() == index) {
                    it.valueRef() = it.row() == it.col() ? 1.0f : 0.0f;
                }
            }
        }
    }
}

std::vector<float> solveStressStrainProblem(const Mesh& mesh, const std::vector<Point>& forces) {
    // fill the right-hand side -- vector of external forces
    Eigen::VectorXf b;
    b.resize(2 * mesh.nodes.size());
    for (uint32_t i = 0; i < mesh.nodes.size(); ++i) {
        b[2 * i + 0] = forces[i][0];
        b[2 * i + 1] = forces[i][1];
    }

    // fill the left-hand side matrix
    std::vector<Eigen::Matrix<float, 3, 6>> gradients;
    std::vector<Eigen::Triplet<float>> triplets;
    computeStiffnessMatrix(mesh, gradients, triplets);

    Eigen::SparseMatrix<float> stiffnessMatrix(2 * mesh.nodes.size(), 2 * mesh.nodes.size());
    stiffnessMatrix.setFromTriplets(triplets.begin(), triplets.end());

    // boundary constraints
    applyBoundaryConditions(stiffnessMatrix, mesh.boundary);

    // solve the linear system
    Eigen::SimplicialLDLT<Eigen::SparseMatrix<float>> solver(stiffnessMatrix);
    Eigen::VectorXf displacements = solver.solve(b);

    // extract the vector of stresses
    std::vector<float> result(mesh.faces.size());
    for (uint32_t i = 0; i < mesh.faces.size(); ++i) {
        const int x1 = 2 * mesh.faces[i][0];
        const int x2 = 2 * mesh.faces[i][1];
        const int x3 = 2 * mesh.faces[i][2];
        Eigen::Matrix<float, 6, 1> delta;
        delta << displacements[x1], displacements[x1 + 1], displacements[x2], displacements[x2 + 1],
            displacements[x3], displacements[x3 + 1];

        Eigen::Vector3f eps = gradients[i] * delta;
        result[i] += 100.f * eps[0];
    }

    return result;
}
