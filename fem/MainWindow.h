#pragma once

#include "../common/Palette.h"
#include "Mesh.h"
#include <algorithm>
#include <wx/dcclient.h>
#include <wx/frame.h>
#include <wx/rawbmp.h>

class MainWindow : public wxFrame {
    Mesh mesh_;
    std::vector<float> u_;
    Palette palette_;

public:
    MainWindow();

    void update(const Mesh& mesh, const std::vector<float>& u);

private:
    void paint();
};
