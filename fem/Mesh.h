#pragma once

#include "../common/Point.h"

class Face {
    std::array<int, 3> idxs_;

public:
    Face() = default;

    Face(const int a, const int b, const int c)
        : idxs_{ a, b, c } {}

    int operator[](const int i) const {
        assert(unsigned(i) < 3);
        return idxs_[i];
    }
};

struct Mesh {
    std::vector<Point> nodes;
    std::vector<Face> faces;
    std::vector<int> boundary;

    std::pair<Point, Point> getExtents() const {
        Point p_min(1.e6f, 1.e6f);
        Point p_max(-1.e6f, -1.e6f);

        for (const Point& p : nodes) {
            p_min = Point(std::min(p_min[0], p[0]), std::min(p_min[1], p[1]));
            p_max = Point(std::max(p_max[0], p[0]), std::max(p_max[1], p[1]));
        }
        return std::make_pair(p_min, p_max);
    }
};

Mesh constructMesh();

std::vector<Point> computeForces(const Mesh& mesh);
