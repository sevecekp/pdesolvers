#pragma once

#include "Particle.h"

class Grid {
    std::vector<std::vector<int>> cells_;
    Point lower_;
    Point upper_;
    int sizeX_;
    int sizeY_;

public:
    Grid(const int sizeX, const int sizeY)
        : sizeX_(sizeX)
        , sizeY_(sizeY) {
        cells_.resize(sizeX_ * sizeY_);
    }

    void build(const std::vector<Particle>& particles) {
        lower_ = Point(1.e6f, 1.e6f);
        upper_ = Point(-1.e6f, -1.e6f);

        for (std::vector<int>& cell : cells_) {
            cell.clear();
        }

        // get extents
        const float eps = 0.02f;
        for (uint i = 0; i < particles.size(); ++i) {
            const Point p = particles[i].r;
            lower_.x() = std::min(lower_.x(), p.x() - eps);
            lower_.y() = std::min(lower_.y(), p.y() - eps);
            upper_.x() = std::max(upper_.x(), p.x() + eps);
            upper_.y() = std::max(upper_.y(), p.y() + eps);
        }

        // fill cells (depends on extents)
        for (uint i = 0; i < particles.size(); ++i) {
            const Point p = particles[i].r;
            int x, y;
            std::tie(x, y) = getCellXy(p);
            cells_[map(x, y)].push_back(i);
        }
    }

    void search(const std::vector<Particle>& particles,
        const int idx,
        const float radius,
        std::vector<int>& neighbors) const {
        neighbors.clear();

        const Point cellSize((upper_.x() - lower_.x()) / sizeX_, (upper_.y() - lower_.y()) / sizeY_);
        const Point query = particles[idx].r;
        int x1, y1;
        std::tie(x1, y1) = getCellXy(query);
        int x2 = x1, y2 = y1;
        Point cellLower = Point(x1 * cellSize.x(), y1 * cellSize.y()) + lower_;
        Point cellUpper = Point(x2 * cellSize.x(), y2 * cellSize.y()) + lower_;
        Point diffLower = query - cellLower;
        Point diffUpper = cellUpper - query;
        while (x2 < sizeX_ - 1 && diffUpper.x() < radius) {
            diffUpper.x() += cellSize.x();
            x2++;
        }
        while (y2 < sizeY_ - 1 && diffUpper.y() < radius) {
            diffUpper.y() += cellSize.y();
            y2++;
        }
        while (x1 > 0 && diffLower.x() < radius) {
            diffLower.x() += cellSize.x();
            x1--;
        }
        while (y1 > 0 && diffLower.y() < radius) {
            diffLower.y() += cellSize.y();
            y1--;
        }

        for (int y = y1; y <= y2; ++y) {
            for (int x = x1; x <= x2; ++x) {
                const std::vector<int>& cell = cells_[map(x, y)];
                for (int i : cell) {
                    if (i != idx && length(particles[i].r - query) < radius) {
                        neighbors.push_back(i);
                    }
                }
            }
        }
    }


private:
    int map(const int x, const int y) const {
        return y * sizeX_ + x;
    }

    std::pair<int, int> getCellXy(const Point& p) const {
        const float relX = (p.x() - lower_.x()) / (upper_.x() - lower_.x());
        const float relY = (p.y() - lower_.y()) / (upper_.y() - lower_.y());
        const int x = std::max(std::min<int>(std::round(relX * sizeX_), sizeX_ - 1), 0);
        const int y = std::max(std::min<int>(std::round(relY * sizeY_), sizeY_ - 1), 0);
        return std::make_pair(x, y);
    }
};
