#pragma once

#include "../common/Point.h"

struct Particle {
    Point r;  // position
    Point v;  // velocity
    Point dv; // acceleration

    float m;    // mass
    float rho;  // density
    float drho; // density derivative
    float p;    // pressure;

    float h;    // smoothing length
    float divv; // velocity divergence
};
