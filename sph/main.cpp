#include "MainWindow.h"
#include "Solver.h"
#include <wx/app.h>
#include <wx/msgdlg.h>

class App : public wxApp {
public:
    App() = default;

private:
    virtual bool OnInit() override;

    virtual int OnExit() override;
};

IMPLEMENT_APP(App);

bool App::OnInit() {
    MainWindow* window = new MainWindow();
    window->Show();

    Boundary boundary({
        Point(-1.5f, -1.5f),
        Point(-1.5f, 1.5f),
        Point(1.5f, 1.5f),
        Point(1.5f, -1.5f),
        Point(-1.5f, -1.5f),
    });

    /* Boundary boundary({
         Point(-3, 0),
         Point(-1, 0.5),
         Point(-0.5, 1.8),
         Point(0.5, 1.8),
         Point(1, 0.5),
         Point(3, 0),
     });*/

    window->viewBoundary(boundary);

    std::vector<Particle> particles = getInitialConditions(2000);
    solve(particles, boundary, [window](const std::vector<Particle>& particles, const float t) { //
        window->update(particles, t);
    });

    return true;
}

int App::OnExit() {
    return 0;
}
