#pragma once

#include "../common/Palette.h"
#include "Particle.h"
#include "Solver.h"
#include <wx/dcclient.h>
#include <wx/frame.h>
#include <wx/rawbmp.h>

class MainWindow : public wxFrame {
    std::vector<Particle> particles_;
    Boundary boundary_;
    float t_ = 0.f;
    Palette palette_;

public:
    MainWindow()
        : wxFrame(nullptr, wxID_ANY, "SPH", wxDefaultPosition, wxSize(1024, 768)) {
        palette_ = Palette({
            { 0.f, Color(0.f, 0.f, 0.5f) },
            { 2.f, Color(0.f, 1.f, 0.f) },
            { 5.f, Color(1.f, 0.f, 0.f) },
            { 10.f, Color(1.f, 1.f, 0.f) },
        });

        Bind(wxEVT_PAINT, [this](wxPaintEvent&) {
            if (particles_.empty()) {
                return;
            }

            wxPaintDC dc(this);
            const wxSize size = dc.GetSize();
            const float scale = 0.25f * size.y;
            const Point offset(0.5f * size.x, 0.5f * size.y);

            dc.SetPen(*wxWHITE_PEN);
            for (int i = 0; i < boundary_.segmentCnt(); ++i) {
                const Point p1 = boundary_.segment(i).p1() * scale + offset;
                const Point p2 = boundary_.segment(i).p2() * scale + offset;
                dc.DrawLine(wxPoint(p1.x(), p1.y()), wxPoint(p2.x(), p2.y()));
            }

            wxPen pen = dc.GetPen();
            wxBrush brush = dc.GetBrush();

            for (uint i = 0; i < particles_.size(); ++i) {
                const Point p = particles_[i].r * scale + offset;
                const float h = 0.35f * particles_[i].h * scale;
                const Color color = palette_(length(particles_[i].v));
                brush.SetColour(color);
                pen.SetColour(color);
                dc.SetBrush(brush);
                dc.SetPen(pen);
                dc.DrawCircle(wxPoint(p.x(), p.y()), std::max(int(h), 1));
            }

            dc.DrawText("t = " + std::to_string(t_) + "s", wxPoint(3, 3));
        });
    }

    void viewBoundary(const Boundary& boundary) {
        boundary_ = boundary;
    }

    void update(const std::vector<Particle>& particles, const float t) {
        particles_ = particles;
        t_ = t;
        this->Refresh();
        wxYield();
    }
};
