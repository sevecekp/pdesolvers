#include "Solver.h"
#include "Grid.h"

OptPoint Segment::project(const Point& p) const {
    const Point dp = p - p1_;
    const float t = dot(dp, dir_);
    const float eps = 0.02f;
    if (t < -eps || t > length(p2_ - p1_) + eps) {
        return nullptr;
    }
    const float sgn = dir_.x() * dp.y() - dir_.y() * dp.x();
    if (sgn > 0.f) {
        return nullptr;
    }
    return dir_ * t - dp;
}

OptPoint Boundary::project(const Point& p) const {
    OptPoint sum(nullptr);
    for (const Segment& segment : segments_) {
        OptPoint proj = segment.project(p);
        if (proj) {
            if (!sum) {
                sum.used = true;
                sum.value() = Point(0, 0);
            }
            sum.value() = sum.value() + proj.value();
        }
    }
    return sum;
}

std::vector<Particle> getInitialConditions(const int N) {
    const float a = 1.f;
    const float b = 2.f;
    const float volume = a * b; // M_PI * radius * radius;
    const float h = sqrt(volume / N);
    const float rho = 1000.f;
    const float m = (rho * volume) / N;
    const float eta = 1.5f;

    const float dx = h;
    const float dy = sqrt(3.f) / 2.f * dx;
    std::vector<Particle> particles;
    int row = 0;

    const Point r0(-1.f, 0.5f);
    for (float y = -0.5f * b; y < 0.5f * b; y += dy, row++) {
        for (float x = -0.5f * a; x < 0.5f * a; x += dx) {
            float delta = 0.f;
            if (row % 2 == 1) {
                delta = 0.5f * dx;
            }

            Particle p;
            p.r = Point(x + delta, y) + r0;
            p.v = Point(0, 0);
            p.dv = Point(0, 0);
            p.m = m;
            p.rho = rho;
            p.p = 0.f;
            p.h = eta * h;
            p.divv = 0.f;
            particles.push_back(p);
        }
    }
    return particles;
}

const float c0 = 20.f;

static float eos(const float rho) {
    const float gamma = 7.f;
    const float rho0 = 1000.f;
    return c0 * c0 * rho0 / gamma * (pow(rho / rho0, gamma) - 1.f);
}

void naiveNeighborSearch(const std::vector<Particle>& particles, const uint i, std::vector<int>& neighbors) {
    neighbors.clear();

    for (uint j = 0; j < particles.size(); ++j) {
        if (i == j) {
            continue;
        }

        const float h_bar = 0.5f * (particles[i].h + particles[j].h);
        if (length(particles[i].r - particles[j].r) < 2.f * h_bar) {
            neighbors.push_back(j);
        }
    }
}

// SPH kernel
float kernel(const Point p, const float h) {
    const float q = length(p) / h;
    return 1.f / M_PI * exp(-q * q) / pow(h, 2);
}

// gradient of the SPH kernel
static Point kernelGrad(const Point& p, const float h) {
    const float q = length(p) / h;
    return p / q * 1.f / M_PI * exp(-q * q) * (-2.f * q) / pow(h, 4);
}

static float av(const Particle& p1, const Particle& p2) {
    const float alpha = 1.f;
    const float beta = 2.f;
    const float eps = 0.01f;

    const float dvdr = dot(p1.v - p2.v, p1.r - p2.r);
    if (dvdr >= 0.f) {
        return 0.f;
    }
    const float hbar = 0.5f * (p1.h + p2.h);
    const float rhobar = 0.5f * (p1.rho + p2.rho);
    const float csbar = c0;
    const float mu = hbar * dvdr / (dot(p1.r - p2.r, p1.r - p2.r) + eps * hbar * hbar);
    return p2.m / rhobar * (-alpha * csbar * mu + beta * mu * mu);
}

void solve(std::vector<Particle> particles,
    const Boundary& boundary,
    std::function<void(const std::vector<Particle>&, float)> onTimestep) {

    const Point g(0.f, 9.81f);
    const float dt = 4.e-4f;

    std::vector<int> neighbors;
    Grid grid(20, 20);

    int refreshIdx = 0;
    for (float t = 0.f; t < 100.f; t += dt) {

        grid.build(particles);

#pragma omp parallel for
        for (uint i = 0; i < particles.size(); ++i) {
            Particle& p1 = particles[i];
            p1.p = std::max(eos(p1.rho), 0.f);
            p1.dv = Point(0.f, 0.f);
            p1.drho = 0.f;
        }

#pragma omp parallel for private(neighbors)
        for (uint i = 0; i < particles.size(); ++i) {

            Particle& p1 = particles[i];

            //naiveNeighborSearch(particles, i, neighbors);
            grid.search(particles, i, 2.f * p1.h, neighbors);

            for (uint j : neighbors) {
                const Particle& p2 = particles[j];

                const float h_bar = 0.5f * (p1.h + p2.h);
                const Point dw = kernelGrad(p1.r - p2.r, h_bar);
                p1.dv = p1.dv - dw * p2.m * (p1.p / (p1.rho * p1.rho) + p2.p / (p2.rho * p2.rho));
                p1.dv = p1.dv - dw * av(p1, p2);
                p1.drho = p1.drho - p2.m * dot(p2.v - p1.v, dw);
            }

            if (OptPoint proj = boundary.project(p1.r)) {
                // const float mag = length(proj.value());
                p1.dv = p1.dv + proj.value() * 1.e5f;
            }
        }

#pragma omp parallel for
        for (uint i = 0; i < particles.size(); ++i) {
            Particle& p1 = particles[i];
            p1.v = p1.v + (p1.dv + g) * dt;
            p1.r = p1.r + p1.v * dt;
            p1.rho = p1.rho + p1.drho * dt;
        }

        if (refreshIdx++ % 20 == 0) {
            onTimestep(particles, t);
        }
    }
}
