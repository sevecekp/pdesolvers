#pragma once

#include "Particle.h"
#include <functional>
#include <vector>

struct OptPoint {
    Point p;
    bool used;

    OptPoint(const Point& p)
        : p(p)
        , used(true) {}

    OptPoint(const std::nullptr_t)
        : used(false) {}

    operator bool() const {
        return used;
    }

    Point& value() {
        return p;
    }
};

class Segment {
    Point p1_;
    Point p2_;
    Point dir_;

public:
    Segment(const Point& p1, const Point& p2)
        : p1_(p1)
        , p2_(p2) {
        dir_ = p2 - p1;
        dir_ = dir_ / length(dir_);
    }

    Point p1() const {
        return p1_;
    }

    Point p2() const {
        return p2_;
    }

    OptPoint project(const Point& p) const;
};

class Boundary {
    std::vector<Segment> segments_;

public:
    Boundary() = default;

    explicit Boundary(const std::vector<Point>& points) {
        for (uint i = 1; i < points.size(); ++i) {
            segments_.push_back(Segment(points[i], points[i - 1]));
        }
    }

    int segmentCnt() const {
        return segments_.size();
    }

    const Segment& segment(const int i) const {
        return segments_[i];
    }

    OptPoint project(const Point& p) const;
};

std::vector<Particle> getInitialConditions(const int N = 1000);

void solve(std::vector<Particle> particles,
    const Boundary& boundary,
    std::function<void(const std::vector<Particle>&, float)> onTimestep);
