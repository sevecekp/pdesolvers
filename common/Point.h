#pragma once

#include <array>
#include <assert.h>
#include <cmath>
#include <tuple>
#include <vector>

class Point {
    std::array<float, 2> v_;

public:
    Point()
        : v_{ 0, 0 } {}

    Point(const float x, const float y)
        : v_{ x, y } {}

    float operator[](const int i) const {
        assert(unsigned(i) < 2);
        return v_[i];
    }

    float x() const {
        return v_[0];
    }

    float& x() {
        return v_[0];
    }

    float y() const {
        return v_[1];
    }

    float& y() {
        return v_[1];
    }

    bool operator==(const Point& other) const {
        return v_[0] == other[0] && v_[1] == other[1];
    }

    bool operator<(const Point& other) const {
        return std::make_tuple(v_[0], v_[1]) < std::make_tuple(other[0], other[1]);
    }

    Point& operator+=(const Point& other) {
        *this = *this + other;
        return *this;
    }
    
    Point& operator-=(const Point& other) {
        *this = *this - other;
        return *this;
    }

    Point operator+(const Point& other) const {
        return Point(v_[0] + other[0], v_[1] + other[1]);
    }

    Point operator-(const Point& other) const {
        return Point(v_[0] - other[0], v_[1] - other[1]);
    }

    Point operator*(const float x) const {
        return Point(v_[0] * x, v_[1] * x);
    }

    Point operator/(const float x) const {
        return Point(v_[0] / x, v_[1] / x);
    }
};

inline float dot(const Point& p1, const Point& p2) {
    return p1[0] * p2[0] + p1[1] * p2[1];
}

inline float length(const Point& p) {
    return sqrt(dot(p, p));
}
