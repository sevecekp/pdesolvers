#pragma once

#include <map>
#include <wx/colour.h>

class Color {
    std::array<float, 3> data_;

public:
    Color() = default;

    Color(const float r, const float g, const float b)
        : data_{ r, g, b } {}

    float operator[](const int idx) const {
        return data_[idx];
    }

    Color operator*(const float s) const {
        return Color(data_[0] * s, data_[1] * s, data_[2] * s);
    }

    Color operator+(const Color& other) const {
        return Color(data_[0] + other[0], data_[1] + other[1], data_[2] + other[2]);
    }

    operator wxColour() const {
        const float r = std::min(255.f, data_[0] * 255);
        const float g = std::min(255.f, data_[1] * 255);
        const float b = std::min(255.f, data_[2] * 255);
        return wxColour((unsigned char)r, (unsigned char)g, (unsigned char)b);
    }
};

class Palette {
    std::map<float, Color> colors_;

public:
    Palette() = default;

    Palette(std::map<float, Color> colors)
        : colors_(std::move(colors)) {}

    Color operator()(const float x) const {
        auto lowerIter = colors_.lower_bound(x);
        if (lowerIter == colors_.end()) {
            return colors_.rbegin()->second;
        } else if (lowerIter == colors_.begin()) {
            return colors_.begin()->second;
        } else {
            const Color c2 = lowerIter->second;
            const float x2 = lowerIter->first;
            lowerIter--;
            const Color c1 = lowerIter->second;
            const float x1 = lowerIter->first;
            const float ratio = (x - x1) / (x2 - x1);
            return c1 * (1.f - ratio) + c2 * ratio;
        }
    }
};
