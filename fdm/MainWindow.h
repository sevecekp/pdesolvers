#pragma once

#include "../common/Palette.h"
#include "Grid.h"
#include <wx/dcclient.h>
#include <wx/frame.h>
#include <wx/rawbmp.h>

class MainWindow : public wxFrame {
    Grid state_;
    float t_ = 0.f;
    Palette palette_;

public:
    MainWindow()
        : wxFrame(nullptr, wxID_ANY, "FDM", wxDefaultPosition, wxSize(1024, 768)) {
        palette_ = Palette({
            { 7.4f, Color(0, 0, 0) },
            { 7.7f, Color(0, 0, 1.f) },
            { 8.f, Color(0.5f, 0.5, 0.5f) },
            { 8.3f, Color(1.f, 0, 0) },
            { 8.6f, Color(1.f, 1.f, 0) },
        });


        Bind(wxEVT_PAINT, [this](wxPaintEvent&) {
            if (state_.empty()) {
                return;
            }

            this->paint();
        });
    }

    void update(const Grid& state, const float t) {
        state_ = state;
        t_ = t;
        this->Refresh();
        wxYield();
    }

private:
    void paint() {
        wxPaintDC dc(this);
        const wxSize size = dc.GetSize();
        wxBitmap bitmap(size.x, size.y, wxBITMAP_SCREEN_DEPTH);
        wxNativePixelData pixelData(bitmap);
        assert(pixelData);

        const float scaleX = float(state_.sizeX() - 1) / size.x;
        const float scaleY = float(state_.sizeY() - 1) / size.y;

#pragma omp parallel for
        for (int y = 0; y < size.y; ++y) {
            wxNativePixelData::Iterator iter(pixelData);
            iter.OffsetY(pixelData, y);
            assert(iter.IsOk());
            for (int x = 0; x < size.x; ++x) {
                const float u = state_.interpolate(x * scaleX, y * scaleY);
                const wxColour color = palette_(u);
                iter.Red() = color.Red();
                iter.Green() = color.Green();
                iter.Blue() = color.Blue();
                ++iter;
            }
        }

        dc.DrawBitmap(bitmap, 0, 0);

        dc.SetPen(*wxBLACK_PEN);
        for (int x = 0; x < state_.sizeX(); ++x) {
            dc.DrawLine(wxPoint(x / scaleX, 0), wxPoint(x / scaleX, size.y));
        }
        for (int y = 0; y < state_.sizeY(); ++y) {
            dc.DrawLine(wxPoint(0, y / scaleY), wxPoint(size.x, y / scaleY));
        }

        dc.DrawText("t = " + std::to_string(t_) + "s", wxPoint(3, 3));
    }
};
