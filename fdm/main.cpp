#include "MainWindow.h"
#include "Simulation.h"
#include <wx/app.h>
#include <wx/msgdlg.h>

class App : public wxApp {
public:
    App() = default;

private:
    virtual bool OnInit() override;

    virtual int OnExit() override;
};

IMPLEMENT_APP(App);

bool App::OnInit() {
    MainWindow* window = new MainWindow();
    window->Show();

    solveWaveEquation([window](const Grid& state, const float t) { window->update(state, t); });

    return true;
}

int App::OnExit() {
    return 0;
}
