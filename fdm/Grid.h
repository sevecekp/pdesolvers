#pragma once

#include <assert.h>
#include <cmath>
#include <vector>

class Grid {
    std::vector<float> data_;
    int sizeX_;
    int sizeY_;

public:
    Grid()
        : sizeX_(0)
        , sizeY_(0) {}

    Grid(const int sizeX, const int sizeY)
        : sizeX_(sizeX)
        , sizeY_(sizeY) {
        data_.resize(sizeX_ * sizeY_);
    }

    void fill(const float value) {
        std::fill(data_.begin(), data_.end(), value);
    }

    float& operator()(const int x, const int y) {
        return this->get(x, y);
    }

    float operator()(const int x, const int y) const {
        return this->get(x, y);
    }

    float& get(const int x, const int y) {
        assert(unsigned(x) < unsigned(sizeX_) && unsigned(y) < unsigned(sizeY_));
        return data_[map(x, y)];
    }

    float get(const int x, const int y) const {
        assert(unsigned(x) < unsigned(sizeX_) && unsigned(y) < unsigned(sizeY_));
        return data_[map(x, y)];
    }

    float interpolate(const float x, const float y) const {
        const float x1 = std::min<float>(x, sizeX_ - 1.00001);
        const float y1 = std::min<float>(y, sizeY_ - 1.00001);
        const int x0 = std::floor(x1);
        const int y0 = std::floor(y1);
        const float dx = x1 - x0;
        const float dy = y1 - y0;
        const float u00 = this->get(x0, y0);
        const float u10 = this->get(x0 + 1, y0);
        const float u01 = this->get(x0, y0 + 1);
        const float u11 = this->get(x0 + 1, y0 + 1);

        return (1.f - dx) * (1.f - dy) * u00 + dx * (1.f - dy) * u10 + (1.f - dx) * dy * u01 + dx * dy * u11;
    }

    int sizeX() const {
        return sizeX_;
    }

    int sizeY() const {
        return sizeY_;
    }

    bool empty() const {
        return data_.empty();
    }

private:
    int map(const int x, const int y) const {
        return x + y * sizeX_;
    }
};
