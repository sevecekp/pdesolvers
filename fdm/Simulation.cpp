#include "Simulation.h"
#include <eigen3/Eigen/Sparse>
#include <iostream>

void solveWaveEquation(std::function<void(const Grid& state, const float t)> onTimestep) {
    const int resX = 300; // resolution in X
    const int resY = 225; // resolution in Y

    const float u0 = 8.f; // initial value
    const float u1 = 6.f; // amplitude of the perturbation
    const int perX = 100; // x of the perturbation center
    const int perY = 110; // y of the perturbation center
    const int perR = 8;   // radius of the perturbation

    const float dt = 0.01f; // time step
    const float dx = 0.5f;  // grid cell size
    const float dx2 = dx * dx;
    const float cs2 = 5.f; // square of sound speed

    // set up initial conditions
    Grid u(resX, resY);
    u.fill(u0);

    for (int y = -perR; y <= perR; ++y) {
        for (int x = -perR; x <= perR; ++x) {
            const float r2 = (x * x + y * y) / (0.25f * perR * perR);
            u(perX + x, perY + y) = u0 + u1 * exp(-r2);
        }
    }
    Grid du_dt(resX, resY);
    du_dt.fill(0.f);
    Grid d2u_dt2(resX, resY);
    d2u_dt2.fill(0.f);


    const int updatePeriod = std::max(int(0.1f / dt), 1);

    std::cout << "Simulation started" << std::endl;
    int idx = 0;
    for (float t = 0; t < 100.f; t += dt, idx++) {
#if 1
        // compute derivatives
#pragma omp parallel for
        for (int y = 1; y < u.sizeY() - 1; ++y) {
            for (int x = 1; x < u.sizeX() - 1; ++x) {
                const float d2u_dx2 = (u(x - 1, y) + u(x + 1, y) - 2.f * u(x, y)) / dx2;
                const float d2u_dy2 = (u(x, y - 1) + u(x, y + 1) - 2.f * u(x, y)) / dx2;
                d2u_dt2(x, y) = cs2 * (d2u_dx2 + d2u_dy2);
            }
        }

        // advance u and du/dt
#pragma omp parallel for
        for (int y = 0; y < u.sizeY(); ++y) {
            for (int x = 0; x < u.sizeX(); ++x) {
                du_dt(x, y) += d2u_dt2(x, y) * dt;
                u(x, y) += du_dt(x, y) * dt;
            }
        }
#else
        const int sizeA = u.sizeX() * u.sizeY();
        std::vector<Eigen::Triplet<float>> A;
        A.reserve(4 * sizeA * sizeA);
        Eigen::VectorXf B(2 * sizeA);


        for (int y = 0; y < u.sizeY(); ++y) {
            for (int x = 0; x < u.sizeX(); ++x) {
                const int i = u.sizeX() * y + x;
                // equation for u^{n+1}
                A.emplace_back(i, i, 1.f);         // value
                A.emplace_back(i, i + sizeA, -dt); // derivative
                B[i] = u(x, y);

                // equation for \dot u_{n+1}
                A.emplace_back(i + sizeA, i + sizeA, 1.f);
                if (x > 0 && x < u.sizeX() - 1 && y > 0 && y < u.sizeY() - 1) {
                    A.emplace_back(i + sizeA, i, 4.f * cs2 / dx2 * dt);
                    A.emplace_back(i + sizeA, i + 1, -cs2 / dx2 * dt);
                    A.emplace_back(i + sizeA, i - 1, -cs2 / dx2 * dt);
                    A.emplace_back(i + sizeA, i + u.sizeX(), -cs2 / dx2 * dt);
                    A.emplace_back(i + sizeA, i - u.sizeX(), -cs2 / dx2 * dt);
                }

                B[i + sizeA] = du_dt(x, y);
            }
        }

        Eigen::SparseMatrix<float> matrix(2 * sizeA, 2 * sizeA);
        matrix.setFromTriplets(A.begin(), A.end());
        // Eigen::SparseLU<Eigen::SparseMatrix<float>, Eigen::COLAMDOrdering<int>> solver;
        Eigen::BiCGSTAB<Eigen::SparseMatrix<float>> solver;
        /*solver.analyzePattern(matrix);
        solver.factorize(matrix);*/
        solver.setTolerance(1.e-6f);
        solver.compute(matrix);
        const Eigen::VectorXf X = solver.solve(B);
        assert(solver.info() == Eigen::Success);

        for (int y = 0; y < u.sizeY(); ++y) {
            for (int x = 0; x < u.sizeX(); ++x) {
                const int i = u.sizeX() * y + x;
                u(x, y) = X[i];
                du_dt(x, y) = X[i + sizeA];
            }
        }

#endif

        if (idx % updatePeriod == 0) {
            onTimestep(u, t);
        }
    }
    std::cout << "Simulation ended" << std::endl;
}
