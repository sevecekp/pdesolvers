#pragma once

#include "Grid.h"
#include <functional>

void solveWaveEquation(std::function<void(const Grid& state, const float t)> onTimestep);
