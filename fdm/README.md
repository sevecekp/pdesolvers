# Solution of a wave equation using finite difference method

Example code written for lecture 'Selected chapters on astrophysics' (fall/winter 2019).

## Requirements

To compile the code, you need:

 - Compiler `gcc >= 6.0`
 - `wxWidgets` library (on Debian/Ubuntu systems, install package `libwxgtk3.0-dev`)
 - `OpenMP` (package `libomp-dev`)

## Compilation

Simply do:
``` make ```

